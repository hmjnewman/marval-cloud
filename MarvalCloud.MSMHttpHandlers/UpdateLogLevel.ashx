﻿<%@ WebHandler Language="C#" Class="UpdateLogLevel" %>

using System;
using System.Configuration;
using System.Security.Principal;
using System.Threading;
using System.Web;
using MarvalSoftware.Security.Authentication;
using MarvalSoftware.ServiceDesk.Facade;
using Serilog.Events;

public class UpdateLogLevel : IHttpHandler
{
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    public void ProcessRequest(HttpContext context)
    {
        if (context.Request.QueryString["accessToken"] == ConfigurationManager.AppSettings["accessToken"])
        {
            const string username = "System";
            Thread.CurrentPrincipal = new Authenticator().GetSystemUserInformation(username, Guid.NewGuid().ToString(), new GenericIdentity(username));
            var adminFacade = new AdminFacade();
            var systemSettings = adminFacade.ViewSystemSettings();
            systemSettings.LogLevel = (LogEventLevel)Int32.Parse(context.Request.QueryString["logLevel"]);
            adminFacade.RecordSystemSettings(systemSettings);
        }
        else
        {
            context.Response.StatusCode = 404;
        }
    }
}
