$ErrorActionPreference = "Stop";

# system variables
$serverUri = $OctopusParameters["Octopus.Web.ServerUri"];
$tenantId = $OctopusParameters["Octopus.Deployment.Tenant.Id"];
$tenantName = $OctopusParameters["Octopus.Deployment.Tenant.Name"];
$projectId = $OctopusParameters["Octopus.Project.Id"];
$environmentId = $OctopusParameters["Octopus.Environment.Id"];
$environmentName = $OctopusParameters["Octopus.Environment.Name"];

# global variables
$azureDatabaseHost = $OctopusParameters["Azure.Database.Host"];
$azureDatabasePort = $OctopusParameters["Azure.Database.Port"];
$azureDatabaseUser = $OctopusParameters["Azure.Database.User"];
$azureDatabasePassword = $OctopusParameters["Azure.Database.Password"];
$elasticCloudApiKey = $OctopusParameters["ElasticCloud.ApiKey"];
$octopusDeployApiKey = $OctopusParameters["OctopusDeploy.ApiKey"];

# tenant variables
$tenantAlias = $OctopusParameters["Tenant.Alias"];

# script variables
$resourceName = "{0}-{1}" -f $tenantAlias, $environmentName.ToLower();

# functions
function generatePassword($size) {
    $tokenSets = @(
        [Char[]]"ABCDEFGHIJKLMNOPQRSTUVWXYZ",
        [Char[]]"abcdefghijklmnopqrstuvwxyz",
        [Char[]]"0123456789",
        [Char[]]"!`"#$%&'()*+,-./:;<=>?@[\]^_`{|}~"
    );
    
    $chars = $tokenSets | ForEach-Object { $_ | Get-Random };

    while ($chars.Count -lt $size) {
        $chars += $tokenSets | Get-Random;
    }

    ($chars | Sort-Object { Get-Random }) -join "";
}

function executeSqlCommand($database, $commandText) {
    $sqlConnection = New-Object System.Data.SqlClient.SqlConnection;
    $sqlConnection.ConnectionString = "Server=$azureDatabaseHost,$azureDatabasePort;Database=$database;User=$azureDatabaseUser;Password=$azureDatabasePassword;Encrypt=true";
    $sqlConnection.Open();
    $sqlCommand = $sqlConnection.CreateCommand();
    $sqlCommand.CommandText = $commandText;
    $sqlCommand.ExecuteNonQuery() | Out-Null;
    $sqlCommand.Dispose();
    $sqlConnection.Dispose();
}

function createDatabase($name, $user) {
    az sql db create --resource-group shared-resources --server marvalcloud --name $name --elastic-pool default | Out-Null;
    executeSqlCommand $name "CREATE USER $user FOR LOGIN $user WITH DEFAULT_SCHEMA = dbo; EXEC sp_addrolemember 'db_owner', '$user';";
}

function setTenantVariable($tenantVariables, $name, $value) {
    $id = ($tenantVariables.ProjectVariables.$projectId.Templates | Where-Object { $_.Name -eq $name }).Id;

    if ($null -eq $tenantVariables.ProjectVariables.$projectId.Variables.$environmentId.$id) {
        $tenantVariables.ProjectVariables.$projectId.Variables.$environmentId | Add-Member -MemberType NoteProperty -Name $id -Value $value;
    } else {
        $tenantVariables.ProjectVariables.$projectId.Variables.$environmentId.$id = $value;
    }
}

"creating database login...";
$databaseUser = $tenantAlias;
$databasePassword = generatePassword 40;
executeSqlCommand "master" ("CREATE LOGIN $databaseUser WITH PASSWORD = '{0}';" -f $databasePassword.Replace("'", "''"));

"creating msm database...";
$databaseMsm = "$resourceName-msm";
createDatabase $databaseMsm $databaseUser;

"creating msm audit database...";
$databaseMsmAudit = "$resourceName-msm-audit";
createDatabase $databaseMsmAudit $databaseUser;

"creating elasticsearch instance...";
$elasticsearchDeployment = Invoke-WebRequest https://bitbucket.org/marvalsoftware/marval-cloud/raw/master/Provisioning/Elasticsearch.json -UseBasicParsing | ConvertFrom-Json;
$elasticsearchDeployment | Add-Member -MemberType NoteProperty -Name "name" -Value $resourceName;
$result = Invoke-WebRequest https://api.elastic-cloud.com/api/v1/deployments -Method "POST" -Headers @{ "Authorization" = "ApiKey $elasticCloudApiKey"; "Content-Type" = "application/json" } -Body ($elasticsearchDeployment | ConvertTo-Json -Depth 8) -UseBasicParsing | ConvertFrom-Json;
$elasticsearchHost, $elasticsearchPort = (([Text.Encoding]::Utf8.GetString([Convert]::FromBase64String($result.resources[0].cloud_id.Split(':')[1])).Split('$') | Select-Object -First 2 | Sort-Object { (--$script:i) }) -join ".").Split(':');
$elasticsearchUsername = $result.resources.credentials.username;
$elasticsearchPassword = $result.resources.credentials.password;

"creating resource group...";
az group create --name $tenantAlias --location uksouth | Out-Null;

"creating redis instance...";
$redisName = "{0}-{1}" -f $resourceName, (New-Guid).ToString("N");
$result = az redis create --resource-group $tenantAlias --name $redisName --location uksouth --sku Standard --vm-size C0 --minimum-tls-version 1.2 | ConvertFrom-Json;
$redisHost = $result.hostName;
$redisPort = $result.sslPort;
$result = az redis list-keys --resource-group $tenantAlias --name $redisName | ConvertFrom-Json;
$redisPassword = $result.primaryKey;

"creating storage account...";
$awsAccessKey = $null;

while ($true) {
    $awsAccessKey = (New-Guid).ToString("N").Substring(0, 24);
    $result = az storage account create --resource-group $tenantAlias --name $awsAccessKey --location uksouth --sku Standard_LRS 2>&1;

    if (!$result -or $result.GetType().Name -ne "ErrorRecord" -or $result.ToString() -ne "ERROR: (StorageAccountAlreadyTaken) The storage account named test is already taken.") {
        break;
    }
}

$awsSecretKey = (az storage account keys list --resource-group $tenantAlias --account-name $awsAccessKey | ConvertFrom-Json)[0].Value;
$awsBucketName = "msm";
az storage container create --name $awsBucketName --account-name $awsAccessKey --account-key $awsSecretKey | Out-Null;

"creating virtual machine...";
az network nsg create --resource-group $tenantAlias --location uksouth --name $resourceName | Out-Null;
az network nsg rule create --resource-group $tenantAlias --nsg-name $resourceName --priority 300 --name HTTP --destination-port-range 80 --protocol Tcp | Out-Null;
az network nsg rule create --resource-group $tenantAlias --nsg-name $resourceName --priority 310 --name HTTPS --destination-port-range 443 --protocol Tcp | Out-Null;
$publicIpId = (az network public-ip create --resource-group $tenantAlias --name $resourceName --allocation-method Static | ConvertFrom-Json).publicIp.id;
az network vnet create --resource-group $tenantAlias --name $resourceName --subnet-name $resourceName | Out-Null;
az network nic create --resource-group $tenantAlias --name $resourceName --vnet-name $resourceName --subnet $resourceName --network-security-group $resourceName --public-ip-address $resourceName | Out-Null;
$virtualMachineSize = if ($environmentName -eq "Staging") { "Standard_B2ms" } else { "Standard_B4ms" };
$virtualMachineAdminPassword = generatePassword 40;
$virtualMachineAdminPassword | az vm create --resource-group $tenantAlias --name $resourceName --size $virtualMachineSize --image win2019datacenter --computer-name $resourceName.Substring(0, 15) --admin-username $tenantAlias --admin-password "@-" --os-disk-name $resourceName --nics $resourceName | Out-Null;
az vm extension set --resource-group $tenantAlias --vm-name $resourceName --publisher Microsoft.Azure.ActiveDirectory --name AADLoginForWindows | Out-Null;

"creating key vault...";
az keyvault create --resource-group $tenantAlias --name $resourceName --enable-rbac-authorization --only-show-errors | Out-Null;
$virtualMachineAdminPassword | az keyvault secret set --vault-name $resourceName --name "virtual-machine-admin-password" --value "@-" | Out-Null;

"creating dns address record...";
$recordSetName = if ($environmentName -eq "Production") { $tenantAlias } else { $resourceName };
$domain = (az network dns record-set a create --resource-group shared-resources --zone-name marval.cloud --name $recordSetName --target-resource $publicIpId | ConvertFrom-Json).fqdn.TrimEnd(".");

"preparing virtual machine...";
(Invoke-WebRequest https://bitbucket.org/marvalsoftware/marval-cloud/raw/master/Provisioning/PrepareVirtualMachine.ps1 -UseBasicParsing).Content | az vm run-command invoke --resource-group $tenantAlias --name $resourceName --command-id RunPowerShellScript --scripts "@-" --parameters "serverUri=$serverUri" "tenantName=$tenantName" "octopusDeployApiKey=$octopusDeployApiKey" "minioGatewayType=azure" "minioRootUser=$awsAccessKey" "minioRootPassword=$awsSecretKey" "virtualMachineName=$resourceName" "domain=$domain" | Out-Null;

while ($true) {
    try {
        "waiting for response...";
        Invoke-WebRequest $domain -TimeoutSec 10 -UseBasicParsing;
    } catch {
        if ($_.Exception.Response) {
            break;
        }
    }
}

"setting octopus deploy variables...";
$tenantVariables = Invoke-WebRequest "$serverUri/api/tenants/$tenantId/variables" -Headers @{ "X-Octopus-ApiKey" = $octopusDeployApiKey } -UseBasicParsing | ConvertFrom-Json;
setTenantVariable $tenantVariables "Domain" $domain;
setTenantVariable $tenantVariables "Database.Host" $azureDatabaseHost;
setTenantVariable $tenantVariables "Database.Port" $azureDatabasePort;
setTenantVariable $tenantVariables "Database.MSM" $databaseMsm;
setTenantVariable $tenantVariables "Database.MSMAudit" $databaseMsmAudit;
setTenantVariable $tenantVariables "Database.User" $databaseUser;
setTenantVariable $tenantVariables "Database.Password" @{ HasValue = $true; NewValue = $databasePassword };
setTenantVariable $tenantVariables "Elasticsearch.Host" $elasticsearchHost;
setTenantVariable $tenantVariables "Elasticsearch.Port" $elasticsearchPort;
setTenantVariable $tenantVariables "Elasticsearch.Username" $elasticsearchUsername;
setTenantVariable $tenantVariables "Elasticsearch.Password" @{ HasValue = $true; NewValue = $elasticsearchPassword };
setTenantVariable $tenantVariables "RedisHost" $redisHost;
setTenantVariable $tenantVariables "RedisPort" $redisPort;
setTenantVariable $tenantVariables "RedisPassword" @{ HasValue = $true; NewValue = $redisPassword };
setTenantVariable $tenantVariables "RedisAdditionalParameters" "ssl=True,sslProtocols=Tls12";
setTenantVariable $tenantVariables "AWSAccessKey" $awsAccessKey;
setTenantVariable $tenantVariables "AWSSecretKey" @{ HasValue = $true; NewValue = $awsSecretKey };
setTenantVariable $tenantVariables "AWSServiceURL" "http://127.0.0.1:9000";
setTenantVariable $tenantVariables "AWSForcePathStyle" "True";
setTenantVariable $tenantVariables "AWSBucketName" $awsBucketName;
setTenantVariable $tenantVariables "ClamAVServer" "localhost";
setTenantVariable $tenantVariables "ClamAVServerPort" "3310";
setTenantVariable $tenantVariables "InstallDir" "C:\Program Files\Marval Software\MSM";
setTenantVariable $tenantVariables "SelfServiceUrl" "https://$domain/MSMSelfService";
setTenantVariable $tenantVariables "SignalRUrl" "https://$domain/MSM/signalr";
Invoke-WebRequest "$serverUri/api/tenants/$tenantId/variables" -Method "PUT" -Headers @{ "X-Octopus-ApiKey" = $octopusDeployApiKey } -Body ($tenantVariables | ConvertTo-Json -Depth 100) -UseBasicParsing | Out-Null;
